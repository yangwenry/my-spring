#FROM maven:3-jdk-8-alpine

#WORKDIR /usr/src/app

#COPY . /usr/src/app
#RUN mvn package

FROM openjdk:8-jdk

WORKDIR /usr/src/app
COPY target/*.jar /usr/src/app/app.jar

ENV PORT 5000
EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
ENTRYPOINT ["java", "-jar", "app.jar"]
