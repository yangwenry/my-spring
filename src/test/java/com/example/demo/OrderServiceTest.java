package com.example.demo;

import com.example.demo.domain.Order;
import com.example.demo.domain.OrderItem;
import com.example.demo.domain.Product;
import com.example.demo.service.OrderItemService;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.vo.Money;
import com.example.demo.vo.OrderStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
//@DataJpaTest
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OrderServiceTest {

    @Autowired
    private static Logger logger = LoggerFactory.getLogger(OrderServiceTest.class);

    @Autowired
    ProductService productService;
    @Autowired
    OrderItemService orderItemService;
    @Autowired
    OrderService orderService;

    @Before
    public void addTestDataForProduct() {
        Product product = new Product();
        product.setName("Swanni coco skin + toner A/C");
        product.setCompany("Swanni coco");
        Money money = new Money(25000, "WON");
        product.setPrice(money);
        productService.saveProduct(product);

        Product product2 = new Product();
        product2.setName("Swanni coco assence A/C");
        product2.setCompany("Swanni coco");
        product2.setPrice(new Money(30000, "WON"));
        productService.saveProduct(product2);

        Product product3 = new Product();
        product3.setName("Swanni coco sun cream");
        product3.setCompany("Swanni coco");
        product3.setPrice(new Money(18000, "WON"));
        productService.saveProduct(product3);

        Product product4 = new Product();
        product4.setName("Swanni coco Mask Pack");
        product4.setCompany("Swanni coco");
        product4.setPrice(new Money(18000, "WON"));
        productService.saveProduct(product4);

        logger.debug("----------------  샘플 데이터를 입력을 완료하였습니다 ---------------------");
    }

    @After
    public void deleteTestDataForProduct() {

        //productService.deleteAll();

        logger.debug("----------------  샘플 데이터를 삭제를 완료하였습니다 ---------------------");
    }

    @Test
    public void getProduct() {

        String name = "Swanni coco skin + toner A/C";
        Product swanni = productService.getProductByName(name);
        assertEquals(name, swanni.getName());

        String companyName = "Swanni coco";
        List<Product> products = productService.getProductByCompany(companyName);
        assertEquals(4, products.size());

        //productService.deleteProduct(1L);
    }

    //@Test
    public void insertOrder() {
        Product product = new Product();
        product.setName("Swanni coco Cleansing");
        product.setPrice(new Money(9000, "WON"));
        product.setCompany("Swanni coco");
        productService.saveProduct(product);

        Order order = new Order();
        order.setOrderStatus(OrderStatus.ORDER);
        order.setOrderNumber("A00001");
        orderService.savecOrder(order);

        OrderItem orderItem = new OrderItem();
        orderItem.setQuantity(3);
        // 연관관계 설정 - 하인에게는 set할 필요없다는 점(양방향 다 하면 좋지만), 하인의 seq가 없어도 된다는 점
        // 1:1일 경우 단방향 설정으로만으로도 해결된다는 점
        orderItem.setOrder(order);
        orderItem.setProduct(product);
        orderItemService.saveOrderItem(orderItem);
    }



}

