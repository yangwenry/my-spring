package com.example.demo.domain;

import com.example.demo.vo.Money;

import javax.persistence.*;

@Entity
@Table(name = "tb_product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Embedded
    private Money price;

    @Column(name = "name", unique = true, length = 100)
    private String name;
    @Column(name = "company", length = 200)
    private String company;

    public Product(String name, Money price, String company) {
        this.name = name;
        this.price = price;
        this.company = company;
    }

    public Product() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


}
