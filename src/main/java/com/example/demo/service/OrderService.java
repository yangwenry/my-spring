package com.example.demo.service;

import com.example.demo.domain.Order;
import com.example.demo.repository.OrderRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;

@Service
@Transactional
public class OrderService {
    @Inject
    private OrderRepository orderRepository;

    public void savecOrder(Order order) {
        orderRepository.save(order);
    }

}
