package com.example.demo.service;

import com.example.demo.domain.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService {
    @Inject
    ProductRepository productRepository;

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public void deleteProduct(Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        Product product = productOptional.get();
        productRepository.delete(product);
    }

    public Product getProductByName(String productName) {
        Product product = productRepository.findByName(productName);
        return product;
    }

    public List<Product> getProductByCompany(String companyName) {
        List<Product> products = productRepository.findByCompany(companyName);
        return products;
    }

    public void deleteAll() {
        productRepository.deleteAll();
    }
}
